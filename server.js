const express = require('express');

const PORT = process.env.PORT || 5000;

const app = express();

app.use(function (request, response) {
  response.status_code = 444;
  request.connection.destroy();
});

const server = app.listen(PORT, function () {
  console.log('Server started on port ' + server.address().port);
});
